#include "file.h"

bool get_image_from_file(char* const filename, struct image* const image) {
FILE* file;
	file = fopen(filename, "rb");
	if (file == NULL){return false;}
	bool read_bmp_a = read_from_bmp(file, image);
	if (read_bmp_a != true){return read_bmp_a;}
	fclose(file);
	return true;
}

bool save_image_to_file(char* const filename, struct image* const image) {
FILE* file;
	file = fopen(filename, "wb");
    if (file == NULL){return false;}
    bool write_bmp_a = read_to_bmp(file, image);
    if (write_bmp_a != false){return write_bmp_a;}
	fclose(file);
	return true;
}
