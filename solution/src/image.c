#include "image.h"
#include "bmp.h"

struct image create_image(size_t width, size_t height) {
 struct image image = {0};
 image.width = width; 
 image.height = height; 
 image.data = malloc(width * height * sizeof(struct pixel));
 return image; 
}

void destroy_image(struct image image) {
	free(image.data);
}

struct image rotation_of_image(struct image* const img) {
    const uint32_t width = img->width;
    const uint32_t height = img->height;
    struct image result = create_image(height, width);
    for (size_t i = 0; i < height; ++i) {
        for(size_t j = 0; j < width; ++j) {
            const struct pixel pixel = get_pixels(img, i, j);
            set_pixels(&result, j, height - i - 1, pixel);
        }
    }


    return result;

}
