#include "file.h"
#include "image.h"
#include <stdio.h>

void std_output(const char *a) {
    fprintf(stdout, "%s", a);
}

void std_error_output(const char *a) {
    fprintf(stderr, "%s", a);
}


int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "%s", "Incorrect format of arguments!");
    } else {
        struct image start = {0};
        struct image res = {0};
        bool read_image_start = get_image_from_file(argv[1], &start);
        if (read_image_start == false) {
            std_error_output("Error while reading file\n");
            return 0;
        } else std_output("Successfully read\n");
        res = rotation_of_image(&start);
        bool set_image_start = save_image_to_file(argv[2], &res);
        if (set_image_start == false) {
            std_error_output("Error while writing into file\n");
            return 0;
        } else std_output("Written successfully\n");
        destroy_image(res);
        destroy_image(start);
    }
    return 0;
}
