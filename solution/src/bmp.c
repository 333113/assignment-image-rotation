#include "bmp.h"

#define TYPE 19778
#define PLANES 1
#define DIB_SIZE 40
#define BPP 24
#define COMP 0
#define X_PPM 0
#define Y_PPM 0
#define IMP_COLORS 0
#define NUM_COLORS 0
#define HEADER_SIZE 54

static bool read_header(FILE *in, struct bmp_header *header) {
    if (fread(header, sizeof(struct bmp_header), 1, in) == 1)
        return true;
    else return false;
}

static uint32_t define_padding(uint32_t width) {
    uint32_t padding;
    if (width % 4 == 0) {
        padding = 0;
    } else padding = 4 * ((width * 3 / 4) + 1) - 3 * width;
    return padding;
}

static bool read_data(FILE *in, const struct image img) {
    uint32_t padding = define_padding(img.width);
    struct pixel *pixels = img.data;
    const size_t width = img.width;
    const size_t height = img.height;
    for (size_t i = 0; i < height; i++) {
        if (fread(pixels + width * i, sizeof(struct pixel), width, in) != width) {
            return false;
        }
        if (fseek(in, padding, SEEK_CUR) != 0) {
            return false;
        }
    }
    return true;
}

static struct bmp_header create_header(const struct image *const img) {
    struct bmp_header result = {0};
    uint32_t padding = define_padding(img->width);
    size_t image_size = sizeof(struct pixel) * img->width * img->height + padding * img->height;
    result.biClrImportant = 0;
    result.bfType = TYPE;
    result.bOffBits = HEADER_SIZE;
    result.biSize = DIB_SIZE;
    result.biWidth = img->width;
    result.biHeight = img->height;
    result.biPlanes = PLANES;
    result.biBitCount = BPP;
    result.biCompression = COMP;
    result.biSizeImage = image_size;
    result.biXPelsPerMeter = X_PPM;
    result.biYPelsPerMeter = Y_PPM;
    result.biClrUsed = NUM_COLORS;
    result.biClrImportant = IMP_COLORS;
    result.bOffBits = sizeof(struct bmp_header);
    result.bFileSize = result.biSizeImage + result.bOffBits;
    return result;

}

static bool write_header(FILE *out, struct image const *img) {
    struct bmp_header header = create_header(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
        return false;
    else return true;
}

static bool write_content(FILE *out, struct image img) {
    uint32_t padding = define_padding(img.width);
    const size_t width = img.width;
    const size_t height = img.height;
    struct pixel *current_pixel = img.data;
    for (size_t i = 0; i < height; i++) {
        if (fwrite(current_pixel, sizeof(struct pixel), width, out) != width)
            return false;
        if (fwrite(current_pixel, 1, padding, out) != padding)
            return false;
        current_pixel = current_pixel + (size_t) width;
    }
    return true;
}

struct pixel get_pixels(const struct image *const img, const size_t row, const size_t col) {
    return img->data[row * img->width + col];
}

void set_pixels(struct image *const img, const size_t row, const size_t col, const struct pixel pixel) {
    img->data[row * img->width + col] = pixel;
}

bool read_to_bmp(FILE *out, struct image const *img) {
    if (write_header(out, img) != true)
        return false;
    return write_content(out, *img);
}

bool read_from_bmp(FILE *in, struct image *img) {
    struct bmp_header *header = malloc(sizeof(struct bmp_header));
    bool header_check = read_header(in, header);
    if (header_check != true)
        return header_check;
    img->width = header->biWidth;
    img->height = header->biHeight;
    img->data = malloc(header->biWidth * header->biHeight * sizeof(struct pixel));
    bool read_check = read_data(in, *img);
    if (!read_check){
        free(img->data);
        return false;
    }
    free(header);
    return read_check;
}
