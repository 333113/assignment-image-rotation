#ifndef FILE_OPS
#define FILE_OPS
#include "bmp.h"
#include <stdbool.h>
#include <stdio.h>

bool get_image_from_file(char* const filename, struct image* const image);
bool save_image_to_file(char* const filename, struct image* const image);
#endif
