#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>


#ifndef BMP
#define BMP
#pragma pack(push, 1)
struct __attribute__((__packed__)) bmp_header {
    uint16_t bfType;
    uint32_t bFileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

#pragma pack(push, 2)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image {
    uint32_t width, height;
    struct pixel *data;
};

struct pixel get_pixels(const struct image* const img, const size_t row, const size_t col);

void set_pixels(struct image* const img, const size_t row, const size_t col, const struct pixel pixel);



struct image create_image(size_t width, size_t height);
bool read_from_bmp(FILE *in, struct image *img);
bool read_to_bmp(FILE *out, struct image const *img);
#endif
