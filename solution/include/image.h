#ifndef IMAGE_STRUCTS
#define IMAGE_STRUCTS
#include "bmp.h"
#include <stdbool.h>

void destroy_image(struct image image);

struct image rotation_of_image(struct image* const img);

#endif
